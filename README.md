Работа с токенами: для создания, получения, изменения и удаления компании 
пользователь должен передать в запросе следующий заголовок: *Authorization: Bearer токен_пользователя*,
где *токен_пользователя* можно получить, передав логин/пароль в post запросе к адресу: *members/uids/*. 
Токен пользователя генерируется с помощью php функии uniqid. Пароль и контрольная сумма хешируются с помощью функции php sha1.
Для тестов создан пользователь с токеном 1.    

Всего потрачено на задание: 5.5 дней.

Как потестировать:

1. git clone https://maxim_boychenko@bitbucket.org/maxim_boychenko/symfony-test.git
2. cd symfony-test
3. cp phpunit.xml.dist phpunit.xml
4. cp .env.dist .env
5. прописать данные для соединения с базой данных в .env и phpunit.xml 
(для .env это DATABASE_URL=pgsql://db_user:db_password@127.0.0.1:5432/db_name, для 
phpunit.xml это <env name="DATABASE_URL" value="" />)
6. создание базы данных: php bin/console d:d:c
7. composer install
8. создание миграции: php bin/console doctrine:migrations:diff
9. выполнение миграции: php bin/console doctrine:migrations:migrate
10. выполнение фикстуры: php bin/console d:f:l
11. теперь можно выполнять тесты: ./vendor/bin/simple-phpunit
12. запуск локального сервера: php bin/console server:start
13. можно открыть документацию к api по адресу: localhost:8000/api/doc
14. как пользоваться документацией: можно выполнить каждый запрос через try it out, для авторизации можно набрать Bearer 1 