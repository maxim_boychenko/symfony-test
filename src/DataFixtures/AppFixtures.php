<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 18.03.18
 * Time: 18:04
 */

namespace App\DataFixtures;

use App\Entity\Campaign;
use App\Entity\CampaignType;
use App\Entity\Member;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $connection = $manager->getConnection();
        if ($connection->getDriver()->getName() == 'pdo_pgsql') {
            $connection->exec("ALTER SEQUENCE members_id_seq RESTART WITH 1;");
            $connection->exec("ALTER SEQUENCE campaigns_id_seq RESTART WITH 1;");
            $connection->exec("ALTER SEQUENCE campaign_types_id_seq RESTART WITH 1;");
        }

        $member = new Member(new Request());
        $member->setName('test_member');
        $member->setEmail('test_member@mail.ru');
        $member->setPassword(123);
        $member->setUid(1);

        $manager->persist($member);

        $campaignType = new CampaignType("campaign type 1", 'enabled');
        $manager->persist($campaignType);

        $campaign = new Campaign($member, $campaignType, 'first campaign', 'campaign is finished');
        $manager->persist($campaign);

        $manager->flush();
    }
}