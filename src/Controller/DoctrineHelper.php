<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 18.03.18
 * Time: 0:24
 */

namespace App\Controller;


use App\Entity\ResponseErrorGenerator;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Common\Persistence\ManagerRegistry;
use FOS\RestBundle\View\View;
use Symfony\Component\Validator\Validation;

class DoctrineHelper
{
    public static function saveEntity(ManagerRegistry $doctrine, $entity)
    {
        $manager = $doctrine->getManager();
        $manager->persist($entity);
        $manager->flush();
    }

    public static function deleteEntity(ManagerRegistry $doctrine, $entity)
    {
        $manager = $doctrine->getManager();
        $manager->remove($entity);
        $manager->flush();
    }

    public static function validateEntity($entity)
    {
        $validator = Validation::createValidatorBuilder()
            ->enableAnnotationMapping()
            ->getValidator();
        $errors = $validator->validate($entity);

        if (count($errors) > 0) {
            $error = ResponseErrorGenerator::getConstraintError($errors->get(0));
            return new View($error->errorData, $error->httpStatusCode);
        }

        return null;
    }

    public static function getEntityByField(ManagerRegistry $doctrine, $class, $fieldName, $fieldValue)
    {
        return $doctrine->getRepository($class)
            ->findOneBy(array($fieldName => $fieldValue));
    }

    public static function getEntitiesByField(ManagerRegistry $doctrine, $class, $fieldName, $fieldValue)
    {
        return $doctrine->getRepository($class)
            ->findBy(array($fieldName => $fieldValue));
    }
}