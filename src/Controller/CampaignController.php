<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 15.03.18
 * Time: 21:25
 */

namespace App\Controller;

use App\Entity\Campaign;
use App\Entity\CampaignType;
use App\Entity\ResponseErrorGenerator;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use App\Entity\Member;
use Symfony\Component\Validator\Validation;
use Swagger\Annotations as SWG;


/**
 * @Rest\Route("/api/v1")
 */
class CampaignController extends FOSRestController
{
    /**
     * @Rest\Post("/campaigns")
     *
     * @SWG\Post(
     *      summary="Добавление кампании пользователем",
     *      description="",
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Кампания для добавления",
     *          required=true,
     *          @SWG\Schema(ref="#definitions/Campaign")
     *      ),
     *      @SWG\Response(
     *          response=201,
     *          description="Кампания пользователя успешно добавлена"
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Ошибка в запросе"
     *      ),
     *      @SWG\Response(
     *          response=500,
     *          description="Внутренняя ошибка сервера"
     *      ),
     *      security={
     *         {
     *             "Bearer": {}
     *         }
     *      }
     * )
     */
    public function createAction(Request $request)
    {
        $doctrine = $this->getDoctrine();

        $authenticateResult = $this->authenticate($request, $doctrine);
        if (is_null($authenticateResult['member'])) {
            return $authenticateResult['view'];
        }

        if (!$this->createActionChecksumIsCorrect($request)) {
            $error = ResponseErrorGenerator::getBadChecksum();
            return new View($error->errorData, $error->httpStatusCode);
        }

        $validateResult = CampaignType::validateCampaignTypeExists($doctrine, $request->get('campaign_type_id'));
        if (is_null($validateResult['campaignType'])) {
            return $validateResult['view'];
        }

        $campaign = new Campaign(
            $authenticateResult['member'],
            $validateResult['campaignType'],
            $request->get('name'),
            $request->get('message_end')
        );

        $errorView = DoctrineHelper::validateEntity($campaign);
        if (isset($errorView)) {
            return $errorView;
        }

        DoctrineHelper::saveEntity($doctrine, $campaign);

        return new View($campaign, Response::HTTP_CREATED);
    }

    private function createActionChecksumIsCorrect(Request $request)
    {
        $checksum = sha1($request->get('name')
            . $request->get('message_end')
            . $request->get('campaign_type_id')
        );

        return  $checksum == $request->get('checksum');
    }

    /**
     * @Rest\Get("/campaigns")
     *
     * @SWG\Get(
     *      summary="Получение всех кампаний, созданных пользователем",
     *      description="",
     *      @SWG\Response(
     *          response=200,
     *          description="Списко кампаний успешно получен"
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Ошибка в запросе"
     *      ),
     *      @SWG\Response(
     *          response=500,
     *          description="Внутренняя ошибка сервера"
     *      ),
     *      security={
     *         {
     *             "Bearer": {}
     *         }
     *      }
     * )
     */
    public function getAllAction(Request $request)
    {
        $doctrine = $this->getDoctrine();

        $authenticateResult = $this->authenticate($request, $doctrine);
        if (is_null($authenticateResult['member'])) {
            return $authenticateResult['view'];
        }

        $campaigns = DoctrineHelper::getEntitiesByField($doctrine, Campaign::class, 'member', $authenticateResult['member']);

        return new View($campaigns, Response::HTTP_OK);
    }

    public function authenticate($request, $doctrine) {
        $authorizationHeader = $request->headers->get('authorization');

        if (empty($authorizationHeader)) {
            $error = ResponseErrorGenerator::getAuthorizationHeaderNotFoundError();
            return array('member' => null, 'view' => new View($error->errorData, $error->httpStatusCode));
        }

        $matches = array();
        preg_match('/Bearer (.+)/', $authorizationHeader, $matches);
        if (count($matches) == 0) {
            $error = ResponseErrorGenerator::getAuthorizationHeaderBadFormatError();
            return array('member' => null, 'view' => new View($error->errorData, $error->httpStatusCode));
        }

        $uid = $matches[1];

        $member = DoctrineHelper::getEntityByField($doctrine, Member::class, 'uid', $uid);

        if (is_null($member)) {
            $error = ResponseErrorGenerator::getAuthorizationIncorrectUidError();
            return array('member' => null, 'view' => new View($error->errorData, $error->httpStatusCode));
        }

        return array('member' => $member, 'view' => null);
    }

    /**
     * @Rest\Get("/campaigns/{campaign}", requirements={"campaign"="\d+"})
     *
     * @SWG\Get(
     *      summary="Получение кампании пользователя",
     *      description="",
     *      @SWG\Parameter(
     *          name="campaign",
     *          in="path",
     *          description="id кампании пользователя",
     *          type="integer",
     *          required=true
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="Кампания успешно получена"
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Ошибка в запросе"
     *      ),
     *      @SWG\Response(
     *          response=500,
     *          description="Внутренняя ошибка сервера"
     *      ),
     *      security={
     *         {
     *             "Bearer": {}
     *         }
     *      }
     * )
     */
    public function getCampaignAction(Request $request, $campaign)
    {
        $doctrine = $this->getDoctrine();

        $getCampaignResult = $this->getCampaignEntity($request, $doctrine, $campaign);
        $campaignEntity = $getCampaignResult['campaign'];
        if (is_null($campaignEntity)) {
            return $getCampaignResult['view'];
        }

        return new View($campaignEntity, Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/campaigns/{campaign}", requirements={"campaign"="\d+"})
     *
     * @SWG\Put(
     *      summary="Изменение кампании пользователем",
     *      description="",
     *      @SWG\Parameter(
     *          name="campaign",
     *          in="path",
     *          description="id кампании пользователя",
     *          type="integer",
     *          required=true
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Новые данные кампании",
     *          required=true,
     *          @SWG\Schema(ref="#definitions/CampaignEdit")
     *      ),
     *      @SWG\Response(
     *          response=201,
     *          description="Кампания пользователя успешно изменена"
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Ошибка в запросе"
     *      ),
     *      @SWG\Response(
     *          response=500,
     *          description="Внутренняя ошибка сервера"
     *      ),
     *      security={
     *         {
     *             "Bearer": {}
     *         }
     *      }
     * )
     */
    public function editCampaignAction(Request $request, $campaign)
    {
        $doctrine = $this->getDoctrine();

        $getCampaignResult = $this->getCampaignEntity($request, $doctrine, $campaign);
        $campaignEntity = $getCampaignResult['campaign'];
        if (is_null($campaignEntity)) {
            return $getCampaignResult['view'];
        }

        if (!$this->editActionChecksumIsCorrect($request)) {
            $error = ResponseErrorGenerator::getBadChecksum();
            return new View($error->errorData, $error->httpStatusCode);
        }

        $campaignEntity->setName($request->get('name'));
        $campaignEntity->setMessageEnd($request->get('message_end'));

        $errorView = DoctrineHelper::validateEntity($campaignEntity);
        if (isset($errorView)) {
            return $errorView;
        }

        DoctrineHelper::saveEntity($doctrine, $campaignEntity);

        return new View($campaignEntity, Response::HTTP_OK);
    }

    private function editActionChecksumIsCorrect(Request $request)
    {
        $checksum = sha1($request->get('name')
            . $request->get('message_end')
        );

        return  $checksum == $request->get('checksum');
    }

    /**
     * @Rest\Delete("/campaigns/{campaign}", requirements={"campaign"="\d+"})
     *
     * @SWG\Delete(
     *      summary="Удаление кампании пользователя",
     *      description="",
     *      @SWG\Parameter(
     *          name="campaign",
     *          in="path",
     *          description="id кампании пользователя",
     *          type="integer",
     *          required=true
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="Кампания успешно удалена"
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Ошибка в запросе"
     *      ),
     *      @SWG\Response(
     *          response=500,
     *          description="Внутренняя ошибка сервера"
     *      ),
     *      security={
     *         {
     *             "Bearer": {}
     *         }
     *      }
     * )
     */
    public function deleteCampaignAction(Request $request, $campaign)
    {
        $doctrine = $this->getDoctrine();

        $getCampaignResult = $this->getCampaignEntity($request, $doctrine, $campaign);
        $campaignEntity = $getCampaignResult['campaign'];
        if (is_null($campaignEntity)) {
            return $getCampaignResult['view'];
        }

        DoctrineHelper::deleteEntity($doctrine, $campaignEntity);

        return new View(array('deleted campaign' => $campaignEntity), Response::HTTP_OK);
    }

    private function getCampaignEntity($request, $doctrine, $campaign)
    {
        $authenticateResult = $this->authenticate($request, $doctrine);
        if (is_null($authenticateResult['member'])) {
            return array('campaign' => null, 'view' => $authenticateResult['view']);
        }

        $campaignEntity = DoctrineHelper::getEntityByField($doctrine, Campaign::class, 'id', $campaign);
        if (is_null($campaignEntity)) {
            $error = ResponseErrorGenerator::getCampaignNotFoundError($campaign);
            return array('campaign' => null, 'view' => new View($error->errorData, $error->httpStatusCode));
        }
        if ($campaignEntity->getMember()->getId() != $authenticateResult['member']->getId()) {
            $error = ResponseErrorGenerator::getCampaignBadMemberError($campaign, $authenticateResult['member']->getId());
            return array('campaign' => null, 'view' => new View($error->errorData, $error->httpStatusCode));
        }

        return array('campaign' => $campaignEntity, 'view' => null);
    }
}