<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 17.03.18
 * Time: 11:23
 */

namespace App\Controller;

use App\Entity\ResponseErrorGenerator;
use FOS\RestBundle\View\View;

class ExceptionController
{
    public function showAction($exception)
    {
        $error = ResponseErrorGenerator::getExceptionError($exception);
        return new View($error->errorData, $error->httpStatusCode);
    }
}