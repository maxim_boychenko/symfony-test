<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 15.03.18
 * Time: 12:38
 */

namespace App\Controller;

use App\Entity\ResponseErrorGenerator;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use Symfony\Component\Validator\Validation;
use App\Entity\Member;
use FOS\RestBundle\Controller\Annotations\Version;
use Swagger\Annotations as SWG;

/**
 * @Rest\Route("/api/v1")
 */
class MemberController extends FOSRestController
{
    /**
     * @Rest\Post("/members")
     *
     * @SWG\Post(
     *      summary="Добавление пользователя",
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Обьект Member для добавления",
     *          required=true,
     *          @SWG\Schema(ref="#definitions/Member")
     *      ),
     *      @SWG\Response(
     *          response=201,
     *          description="Пользователь успешно добавлен"
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Ошибка в запросе"
     *      ),
     *      @SWG\Response(
     *          response=500,
     *          description="Внутренняя ошибка сервера"
     *      )
     * )
     */
    public function createAction(Request $request)
    {
        $doctrine = $this->getDoctrine();

        if (!Member::checksumIsCorrect($request)) {
            $error = ResponseErrorGenerator::getBadChecksum();
            return new View($error->errorData, $error->httpStatusCode);
        }

        $member = new Member($request);
        $errorView = DoctrineHelper::validateEntity($member);
        if (isset($errorView)) {
            return $errorView;
        }

        $uniqueFiledError = $member->validateUniqueFields($doctrine);
        if ($uniqueFiledError != null) {
            return new View($uniqueFiledError->errorData, $uniqueFiledError->httpStatusCode);
        }

        DoctrineHelper::saveEntity($doctrine, $member);

        return new View($member, Response::HTTP_CREATED);
    }

    /**
     * @Rest\Post("/members/uids")
     *
     * @SWG\Post(
     *      summary="Получение токена пользователя",
     *      description="Токен необходим для выполнения дальнейших запросов",
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="email и пароль пользователя",
     *          required=true,
     *          @SWG\Schema(ref="#definitions/MemberUidRequest")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="Токен uid успешно получен"
     *      ),
     *      @SWG\Response(
     *          response=400,
     *          description="Ошибка в запросе"
     *      ),
     *      @SWG\Response(
     *          response=500,
     *          description="Внутренняя ошибка сервера"
     *      )
     * )
     */
    public function getUidAction(Request $request)
    {
        $email = $request->get('email');
        if (empty($email)) {
            $error = ResponseErrorGenerator::getLostParameterError("email", "email should not be blank");
            return new View($error->errorData, $error->httpStatusCode);
        }

        $password = $request->get('password');
        if (empty($password)) {
            $error = ResponseErrorGenerator::getLostParameterError("password", "password should not be blank");
            return new View($error->errorData, $error->httpStatusCode);
        }

        $member = DoctrineHelper::getEntityByField($this->getDoctrine(), Member::class, 'email', $email);
        if (empty($member) || $member->getPassword() != sha1($password)) {
            $error = ResponseErrorGenerator::getIncorrectEmailPasswordError();
            return new View($error->errorData, $error->httpStatusCode);
        }

        return new View(
            array(
                'email' => $member->getEmail(),
                'uid' => $member->getUid()
            ),
            Response::HTTP_OK
        );
    }
}