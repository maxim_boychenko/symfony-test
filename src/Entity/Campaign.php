<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CampaignRepository")
 * @ORM\Table(name="campaigns")
 */
class Campaign
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Member")
     * @ORM\JoinColumn(name="members_id", referencedColumnName="id")
     */
    private $member;

    /**
     * @ORM\ManyToOne(targetEntity="CampaignType")
     * @ORM\JoinColumn(name="campaign_type_id", referencedColumnName="id")
     */
    private $campaignType;

    /**
     * @ORM\Column(type="string", length=150, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=200, name="message_end", nullable=true)
     */
    private $messageEnd;

    /**
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     * @Assert\NotBlank()
     */
    private $createdAt;

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getMessageEnd()
    {
        return $this->messageEnd;
    }

    public function setMessageEnd($messageEnd)
    {
        $this->messageEnd = $messageEnd;
    }

    public function getMember()
    {
        return $this->member;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCampaignType()
    {
        return $this->campaignType;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function __construct($member, $campaignType, $name, $messageEnd)
    {
        $this->member = $member;
        $this->campaignType = $campaignType;
        $this->name = $name;
        $this->messageEnd = $messageEnd;
        $this->createdAt = new \DateTime();
    }
}
