<?php

namespace App\Entity;

use App\Controller\DoctrineHelper;
use App\Entity\ResponseErrorGenerator;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MemberRepository")
 * @ORM\Table(name="members")
 */
class Member
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=false, unique=true)
     * @Assert\Email()
     * @Assert\NotBlank()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     * @JMS\Exclude()
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=100, nullable=false, unique=true)
     * @Assert\NotBlank()
     * @JMS\Exclude()
     */
    private $uid;

    /**
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     * @Assert\NotBlank()
     */
    private $createdAt;

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getUid()
    {
        return $this->uid;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function setPassword($password)
    {
        $this->password = sha1($password);
    }

    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function __construct(Request $request)
    {
        $this->name = $request->get('name');
        $this->email = $request->get('email');
        $this->phone = $request->get('phone');
        $this->setPassword($request->get('password'));
        $this->createdAt = new \DateTime();
        $this->uid = uniqid();
    }

    public function validateUniqueFields(ManagerRegistry $doctrine)
    {
        if (!empty(DoctrineHelper::getEntityByField($doctrine, self::class, 'email', $this->email))) {
            return ResponseErrorGenerator::getBadValidParameterError('email', "email $this->email is already exist");
        }

        if (!empty(DoctrineHelper::getEntityByField($doctrine, self::class, 'uid', $this->uid))) {
            return ResponseErrorGenerator::getBadValidParameterError('uid', "uid $this->uid is already exist");
        }

        return null;
    }

    public static function checksumIsCorrect($request)
    {
        $checksum = sha1($request->get('name')
            . $request->get('email')
            . $request->get('phone')
            . $request->get('password')
        );

        return  $checksum == $request->get('checksum');
    }
}
