<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 16.03.18
 * Time: 8:01
 */

namespace App\Entity;


class ResponseError
{
    public $errorData;
    public $httpStatusCode;

    public function __construct($errorData, $httpStatusCode)
    {
        $this->errorData = $errorData;
        $this->httpStatusCode = $httpStatusCode;
    }
}