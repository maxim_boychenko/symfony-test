<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 16.03.18
 * Time: 1:21
 */

namespace App\Entity;

use Doctrine\Common\Persistence\ManagerRegistry;
use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\ConstraintViolation;

class ResponseErrorGenerator
{
    public static function getExceptionError(Exception $exception)
    {
        return self::getError(
            Response::HTTP_INTERNAL_SERVER_ERROR,
            $exception->getMessage() . " File: " . $exception->getFile() . " Line: " . $exception->getLine(),
            "Internal server error occured",
            $exception->getCode(),
            $exception->getTraceAsString()
        );
    }

    public static function getConstraintError(ConstraintViolation $error)
    {
        $parameter = $error->getPropertyPath();

        if ($error->getConstraint() instanceof NotBlank) {
            return self::getLostParameterError($parameter, "$parameter should not be blank");
        }

        return self::getBadValidParameterError($parameter, $error->getMessage());
    }

    public static function getLostParameterError($parameterName, $message)
    {
        return self::getError(Response::HTTP_BAD_REQUEST, "Lost parameter $parameterName", "Lost parameter $parameterName", '001', $message);
    }

    public static function getBadValidParameterError($parameterName, $message)
    {
        return self::getError(Response::HTTP_BAD_REQUEST, "Bad valid $parameterName", "Bad valid $parameterName", '002', $message);
    }



    public static function getIncorrectEmailPasswordError()
    {
        return self::getErrorOneMessage(
            Response::HTTP_BAD_REQUEST,
            "Email or password is incorrect",
            "003"
        );
    }

    public static function getError($httpStatusCode, $developerMessage, $userMessage, $errorCode, $moreInfo)
    {
        return new ResponseError(
            array(
                "status" => "$httpStatusCode",
                "developerMessage" => $developerMessage,
                "userMessage" => $userMessage,
                "errorCode" => $errorCode,
                "moreInfo" => $moreInfo
            ),
            $httpStatusCode
        );
    }

    public static function getErrorOneMessage($httpStatusCode, $message, $errorCode)
    {
        return new ResponseError(
            array(
                "status" => "$httpStatusCode",
                "developerMessage" => $message,
                "userMessage" => $message,
                "errorCode" => $errorCode,
                "moreInfo" => $message
            ),
            $httpStatusCode
        );
    }

    public static function getAuthorizationHeaderNotFoundError()
    {
        return self::getErrorOneMessage(Response::HTTP_BAD_REQUEST, "Authorization header could not be blank", "004");
    }

    public static function getAuthorizationHeaderBadFormatError()
    {
        return self::getErrorOneMessage(Response::HTTP_BAD_REQUEST, "Authorization header has invalid format", "005");
    }

    public static function getAuthorizationIncorrectUidError()
    {
        return self::getErrorOneMessage(Response::HTTP_BAD_REQUEST, "Bad authorization header token, member with this token not found", "006");
    }

    public static function getAuthorizationIncorrectCampaignTypeError()
    {
        return self::getErrorOneMessage(Response::HTTP_BAD_REQUEST, "Bad campaign_type_id value, campaign type with this campaign_type_id not found", "007");
    }

    public static function getCampaignNotFoundError($campaign)
    {
        return ResponseErrorGenerator::getErrorOneMessage(Response::HTTP_BAD_REQUEST, "Campaign with id=$campaign not found", "008");
    }

    public static function getCampaignBadMemberError($campaign, $memberId)
    {
        return ResponseErrorGenerator::getErrorOneMessage(Response::HTTP_BAD_REQUEST, "Member with id=$memberId cant view/edit/delete campaign with id=$campaign", "009");
    }

    public static function getBadChecksum()
    {
        return ResponseErrorGenerator::getErrorOneMessage(Response::HTTP_BAD_REQUEST, "Bad checksum", "010");
    }
}