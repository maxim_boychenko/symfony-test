<?php

namespace App\Entity;

use App\Controller\DoctrineHelper;
use Doctrine\ORM\Mapping as ORM;
use App\DBAL\Types\StatusType;
use FOS\RestBundle\View\View;
use Fresh\DoctrineEnumBundle\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolation;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CampaignTypeRepository")
 * @ORM\Table(name="campaign_types")
 */
class CampaignType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(name="status", type="StatusType", nullable=false)
     * @DoctrineAssert\Enum(entity="App\DBAL\Types\StatusType")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", name="created_at", nullable=false)
     * @Assert\NotBlank()
     */
    private $createdAt;

    public function __construct($name, $status)
    {
        $this->name = $name;
        $this->status = $status;
        $this->createdAt = new \DateTime();
    }

    public static function validateCampaignTypeExists($doctrine, $id)
    {
        $campaignType = DoctrineHelper::getEntityByField($doctrine, CampaignType::class, 'id', $id);

        if (is_null($campaignType)) {
            $error = ResponseErrorGenerator::getAuthorizationIncorrectCampaignTypeError();
            return array('campaignType' => null, 'view' => new View($error->errorData, $error->httpStatusCode));
        }

        return array('campaignType' => $campaignType, 'view' => null);
    }
}
