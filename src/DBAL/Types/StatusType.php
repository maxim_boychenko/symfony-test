<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 14.03.18
 * Time: 11:56
 */

namespace App\DBAL\Types;

use Fresh\DoctrineEnumBundle\DBAL\Types\AbstractEnumType;

final class StatusType extends AbstractEnumType
{
    public const ENABLED = 'enabled';
    public const DISABLED = 'disabled';

    protected static $choices = [
        self::ENABLED => 'enabled',
        self::DISABLED => 'disabled'
    ];
}