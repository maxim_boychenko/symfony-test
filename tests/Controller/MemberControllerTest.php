<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 18.03.18
 * Time: 14:33
 */

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class MemberControllerTest extends WebTestCase
{
    public function testCreateMember()
    {
        $client = static::createClient();

        $requestMember = $this->generateMember();
        $requestMember['checksum'] = sha1($requestMember['name']
            . $requestMember['email']
            . $requestMember['phone']
            . $requestMember['password']
        );

        $client->request('POST', '/api/v1/members',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode($requestMember)
        );

        $this->assertEquals(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());
    }

    public function testGetUid()
    {
        $client = static::createClient();

        $client->request('POST', '/api/v1/members/uids',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(
                array(
                    'email' => 'test_member@mail.ru',
                    'password' => '123'
                )
            )
        );

        $responseContent = json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals('test_member@mail.ru', $responseContent['email']);
        $this->assertTrue(isset($responseContent['uid']));
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function generateMember()
    {
        $uniqid = uniqid();
        return array(
            'name' => "test$uniqid",
            'password' => '123',
            'phone' => '12345',
            'email' => "test$uniqid@mail.ru"
        );
    }
}