<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 19.03.18
 * Time: 0:54
 */

namespace App\Tests\Controller;

use App\Controller\DoctrineHelper;
use App\Entity\Campaign;
use App\Entity\CampaignType;
use App\Entity\Member;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class CampaignControllerTest extends WebTestCase
{
    private $doctrine;

    public function setUp() {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->doctrine = static::$kernel->getContainer()->get('doctrine');
    }

    public function testCreateCampaign()
    {
        $client = static::createClient();

        $campaign = array(
            'name' => 'campaign' . uniqid(),
            'message_end' => 'message',
            'campaign_type_id' => '1'
        );
        $campaign['checksum'] = sha1($campaign['name']
            . $campaign['message_end']
            . $campaign['campaign_type_id']
        );

        $client->request('POST', '/api/v1/campaigns',
            array(),
            array(),
            array(
                'CONTENT_TYPE' => 'application/json',
                'HTTP_AUTHORIZATION' => 'Bearer 1'
            ),
            json_encode($campaign)
        );

        $this->assertEquals(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());
    }

    public function testGetCampaign()
    {
        $client = static::createClient();

        $client->request('GET', '/api/v1/campaigns/1',
            array(),
            array(),
            array(
                'CONTENT_TYPE' => 'application/json',
                'HTTP_AUTHORIZATION' => 'Bearer 1'
            )
        );

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testEditCampaign()
    {
        $client = static::createClient();

        $campaign = array(
            'name' => 'campaign' . uniqid(),
            'message_end' => 'message' . uniqid(),
        );
        $campaign['checksum'] = sha1($campaign['name']
            . $campaign['message_end']
        );

        $client->request('PUT', '/api/v1/campaigns/1',
            array(),
            array(),
            array(
                'CONTENT_TYPE' => 'application/json',
                'HTTP_AUTHORIZATION' => 'Bearer 1'
            ),
            json_encode($campaign)
        );

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    public function testDeleteCampaign()
    {
        $campaign = new Campaign(
            DoctrineHelper::getEntityByField($this->doctrine, Member::class, 'id', 1),
            DoctrineHelper::getEntityByField($this->doctrine, CampaignType::class, 'id', 1),
            'campaign',
            'message'
        );
        DoctrineHelper::saveEntity($this->doctrine, $campaign);

        $client = static::createClient();

        $client->request('DELETE', "/api/v1/campaigns/{$campaign->getId()}",
            array(),
            array(),
            array(
                'CONTENT_TYPE' => 'application/json',
                'HTTP_AUTHORIZATION' => 'Bearer 1'
            )
        );

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}