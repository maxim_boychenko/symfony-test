<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 19.03.18
 * Time: 19:21
 */

namespace App\Tests\Entity;

use App\Entity\Member;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class MemberTest extends TestCase
{
    public function testMemberConstructor()
    {
        $name = 'test name';
        $email = 'email@mail.ru';
        $phone = '123-11-11';
        $password = '12345';

        $request = array(
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'password' => $password
        );

        $member = new Member(new Request(array(), $request));

        $this->assertEquals($name, $member->getName());
        $this->assertEquals($email, $member->getEmail());
        $this->assertEquals($phone, $member->getPhone());
        $this->assertEquals(sha1($password), $member->getPassword());
        $this->assertTrue($member->getCreatedAt() instanceof \DateTime);
        $this->assertTrue(!empty($member->getUid()));
    }
}