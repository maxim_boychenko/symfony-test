<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 19.03.18
 * Time: 19:48
 */

namespace App\Tests\Entity;


use App\Entity\Campaign;
use App\Entity\CampaignType;
use App\Entity\Member;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class CampaignTest extends TestCase
{
    public function testCampaignConstructor()
    {
        $name = 'test name';
        $email = 'email@mail.ru';
        $phone = '123-11-11';
        $password = '12345';

        $request = array(
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'password' => $password
        );

        $member = new Member(new Request(array(), $request));

        $campaignType = new CampaignType("campaign type 1", 'enabled');

        $campaignName = 'first campaign';
        $campaignMessageEnd = 'campaign is finished';
        $campaign = new Campaign($member, $campaignType, $campaignName, $campaignMessageEnd);

        $this->assertEquals($member, $campaign->getMember());
        $this->assertEquals($campaignType, $campaign->getCampaignType());
        $this->assertEquals($campaignName, $campaign->getName());
        $this->assertEquals($campaignMessageEnd, $campaign->getMessageEnd());
        $this->assertTrue($campaign->getCreatedAt() instanceof \DateTime);
    }
}